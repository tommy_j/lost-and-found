<?php

include_once '../includes/connections/connect.php';


session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)  {}
else {
     header("Location: ../public_html/index.php");
}

if(isset($_GET['picture'])) {

    $pic = $_GET['picture'];

    echo '<script>';
    echo 'var pic = ' . json_encode($pic) . ';';
    echo '</script>';

} else header("Location: loggedin.php");

?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <link href="../includes/style/style.css" rel="stylesheet" type="text/css" />

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGJx81aQxJhFhtQztuj8Ow2eE_u3bNg4M&callback=initialize"async defer></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


    <script src="../includes/script/script2.js" ></script>


</head>
<title>Lost & Found</title>
<body>
<main class="wrapper">

<header>
    <div class="header-image">
        <img id="header-image" src="images/header.jpg" alt="header" />
    </div>
</header>

<nav class="nav">
    <a href="index.php" id="myBtn11" class="button" >Home</a>
    <a href="#" class="dummylink"></a>
    <a href="#" class="dummylink"></a>

    <div class="dropdown">
        <a  >User</a>
        <div class="dropdown-content">
            <a href="../includes/connections/logout.php"><b>Log out</b></a>
        </div>
    </div>
</nav>






<div class="wrapper2">

    <selection class="item3">

    <div class="container">
        <div id="map_container"></div>
        <div id="googleMap" class="googleMap_1"></div>
    </div>


        </selection>

    <selection class="item2">


        <h3> <div id="instructions">Instructions:</div></h3>
            <h3><div id="fields" style="color:#DF0101">Type in an address to start.</div></h3>

            <fieldset>
                Address:
                <label>
                    <input type="textbox" class="cont" id="address"  placeholder="type any keywords">
                </label>
                <p>
                <button class="button" id="myBtnn" class="button" onclick="codeAddress()" ><span>Search </span></button>
                </p>
                Comment:
                <label>
                    <input type="textbox" class="cont" id="comment"  placeholder="comment">
                </label>
                Email:
                <label>
                    <input type="textbox" class="cont" id="email"  placeholder="email" >
                </label>
                Phone:
                <label>
                    <input type="textbox" class="cont" id="number"  placeholder="phone number" >
                </label>

                <label>
                    <form >
                        <input type="radio"  name="radiotype" id="type1" value="Found" checked="checked"/> Found item
                        <input type="radio"  name="radiotype" id="type2" value="Lost" /> Lost item<br>
                    </form>
                </label>

                <p>
                    <input type="button" class="button"  id="closeBtn" value="Save" disabled>
                </p>

            </fieldset>


    </selection>

</div>



<footer >
    &copy; Lost & Found 2017 All rights reserved.
</footer>


</main>

</body>
</html>
