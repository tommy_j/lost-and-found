<?php

include_once '../includes/connections/connect.php';
session_start();


if  (isset($_SESSION['loggedin']) && $_SESSION['loggedin']) {

    header("Location: ../public_html/loggedin.php");
}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <link href="../includes/style/style.css" rel="stylesheet" type="text/css" />
    <link href="../includes/style/image.css" rel="stylesheet" type="text/css" />


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGJx81aQxJhFhtQztuj8Ow2eE_u3bNg4M&callback=initMap"async defer></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


    <script src="../includes/script/script.js"></script>


</head>
<title>Lost & Found</title>
<body >

<main class="wrapper">

<header>
    <div class="header-image">
        <img id="header-image" src="images/header.jpg" alt="header" />
    </div>
</header>

    <nav class="nav">
    <a href="index.php" id="myBtn11" class="button" >Home</a>
    <a id="myBtn22" class="button" >About</a>
        <a href="#" class="dummylink"></a>
    <a href="login.php" id="myBtn111" class="button" >Login</a>
    </nav>


    <div class="container">
        <div id="map_container"></div>
        <div id="googleMapp"></div>
    </div>


    <div id="myModal1" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close1">×</span>
            <h2>About</h2>
        </div>
        <div class="modal-body">
            <br>
            Lost & Found was launched to help people find their lost items.<br><br>
            The map has two different markers: red for lost items and blue for found items.<br><br>
            It is very easy to use: just mark the lost or found item on the map, add a picture, write a comment
            and type in your contact information.<br><br>
            Hope you will find your lost items!<br><br><br>
        </div>

        <div class="modal-footer">
        </div>
    </div>

</div>


    <div id="myModal" class="modal">
        <span class="close">&times;</span>
        <img class="modal-content" id="img01">
    </div>



<footer >
    &copy; Lost & Found 2017 All rights reserved.
</footer>


</main>



<script>



    var modal1 = document.getElementById('myModal1');
    var btn22 = document.getElementById("myBtn22");

    var span1 = document.getElementsByClassName("close1")[0];

    btn22.onclick = function() {

        modal1.style.display = "block";
    }
    span1.onclick = function() {
        modal1.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal1) {
            modal1.style.display = "none";
        }
    }




</script>

</body>
</html>
