<?php

include_once '../includes/connections/connect.php';


session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)  {}
else {
     header("Location: ../public_html/index.php");
}

if(isset($_GET['error'])) {



    $error = $_GET['error'];

} else $error = '';

?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <link href="../includes/style/style.css" rel="stylesheet" type="text/css" />


</head>
<title>Lost & Found</title>
<body >


<main class="wrapper">

    <header>
        <div class="header-image">
            <img id="header-image" src="images/header.jpg" alt="header" />
        </div>
    </header>

    <nav class="nav">
        <a href="index.php" id="myBtn11" class="button" >Home</a>
        <a href="#" class="dummylink"></a>
        <a href="#" class="dummylink"></a>

        <div class="dropdown">
            <a  >User</a>
            <div class="dropdown-content">
                <a href="../includes/connections/logout.php"><b>Log out</b></a>
            </div>
        </div>
    </nav>

    <section class="item">

        <h3><div id ="error" style="color:#DF0101">  <?php echo $error ?>  </div></h3>
        <form action="../includes/connections/upload.php" method="post" enctype="multipart/form-data">

            <fieldset>

                <legend>Upload a picture</legend>
                <p>
                <label class="custom-file-upload">
                    Click to upload
                    <input type="file" name="fileToUpload" id="fileToUpload">
                    <p>
                    <input type="submit" value="Upload Image" class="button_short" name="submit">
                    </p>
                </label>
                <p>

            </fieldset>
            <p>
                Or skip it:
                <a href="new2.php?picture=null" id="myBtn11" >Skip</a>
            </p>
        </form>


    </section>






    <footer >
        &copy; Lost & Found 2017 All rights reserved.
    </footer>

</main>


</body>
</html>
