<?php

include_once '../includes/connections/connect.php';

session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    header("Location: ../public_html/loggedin.php");
}


?>


<!doctype html>
<html>

<head>
    <title>LostAndFound</title>
    <meta charset = "UTF-8">

    <link href="../includes/style/style.css" rel="stylesheet" type="text/css" />



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../includes/script/register.js"></script>


</head>
<body>
<main class="wrapper">

    <header>
        <div class="header-image">
            <img id="header-image" src="images/header.jpg" alt="header" />
        </div>
    </header>


    <nav class="nav">


        <a href="index.php" class="button">Go back</a>
        <a href="#" class="dummylink"></a>
        <a href="#" class="dummylink"></a>
        <a href="#" class="dummylink"></a>

    </nav>




                            <section class="item">
                                <h2> Lost & Found </h2>

                                <h3><div id="result" style="color:#DF0101"></div></h3>
                                <form >

                                    <fieldset>

                                        <legend>Your Details</legend>
                                        Userame:
                                        <label>
                                            <input class="cont" type="text" id="username" />
                                        </label>
                                        Email:
                                        <label>
                                            <input class="cont" type="text" id="email" />
                                        </label>
                                        Password:
                                        <label>
                                            <input class="cont" type="password" id="password1"/>
                                        </label>
                                        Confirm password:
                                        <label>
                                            <input class="cont" type="password" id="password2"/>
                                        </label>

                                        <p>
                                            <input type="button" class="button" id="log" value="Register" >
                                        </p>

                                    </fieldset>
                                </form>

                            </section>


    <footer>
        &copy; Lost & Found 2017 All rights reserved.
    </footer>

</main>
</body>
</html>