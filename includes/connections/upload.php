
<?php



session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)  {}
else {
    header("Location: ../public_html/index.php");
}


$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
    } else {
        $mes= "File is not an image.";
        header("Location: ../public_html/new1.php?error=$mes");
    }
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    $mes= "Sorry, your file is too large.";
    header("Location: ../public_html/new1.php?error=$mes");
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
    $mes= "Only JPG, GIF, PNG files are allowed.";
    header("Location: ../public_html/new1.php?error=$mes");
} else {
    // Check if file already exists and rename it
    if (file_exists($target_file)) {

        $i = 1;
        while(file_exists($target_file))
        {
            $file = pathinfo($target_file);
            $target_file = $target_dir . $file['filename']. '('.$i.')'  . '.' . $file['extension'];
            $i++;
        }



        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

            $name = basename($target_file);
            header("Location: ../public_html/new2.php?picture=$name");

        } else {
            echo "Sorry, there was an error uploading your file.";

        }
    }else {

            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

                $name = basename($target_file);
                header("Location: ..public_html/new2.php?picture=$name");

            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }

}
?>
