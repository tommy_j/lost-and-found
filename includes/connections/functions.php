<?php


include_once 'connect.php';

$con = $mysqli;


if (isset($_GET['funct']) && method_exists('PublicView',$_GET['funct'])){
    $view = new PublicView();

    $func =$_GET['funct'];

    $view->{$func}($con);
} else {
    echo 'error_Function not found';
}



class PublicView
{

    function insertData($con)
    {
        if (isset($_GET['address'])) {

            $typ = $_GET['type'];
            $addr = mysqli_real_escape_string($con,$_GET['address']);
            $xco = $_GET['xcoord'];
            $yco = $_GET['ycoord'];
            $comm = mysqli_real_escape_string($con,$_GET['comment']);
            $emai = mysqli_real_escape_string($con,$_GET['email']);
            $num = $_GET['telephone'];
            $pict = mysqli_real_escape_string($con,$_GET['pic']);




            session_start();
            $user_id = $_SESSION['user_id'];

            if ($emai == '') {
                $emai = '-';
            }

            if($num == '') {
                $num = '-';
            }


            if ($pict == 'null') {

                $sql0 = "INSERT INTO locations (type ,address, xcoord, ycoord, comment, email, telephone, user, picture)
                    VALUES ('$typ', '$addr', '$xco','$yco','$comm','$emai','$num','$user_id', '1')";


                if (mysqli_query($con, $sql0)) {
                    echo 1;
                    $con->close();
                } else {
                    echo "Error: ". mysqli_query($con, $sql0);
                    $con->close();
                }


            } else {

                $sql = "INSERT INTO `pictures`(`user`, `picture`) VALUES ('$user_id','$pict')";
                if (mysqli_query($con, $sql)) {
                    $sql1 = "SELECT `picture_id` FROM pictures WHERE `picture` = '$pict'";
                    $result = mysqli_query($con, $sql1);
                }
                if ($result) {


                    $pic_id = mysqli_fetch_object($result)->picture_id;
                }
                $sql2 = "INSERT INTO locations (type ,address, xcoord, ycoord, comment, email, telephone, user, picture)
                            VALUES ('$typ', '$addr', '$xco','$yco','$comm','$emai','$num','$user_id', '$pic_id')";


                if (mysqli_query($con, $sql2)) {
                    echo 1;
                    $con->close();
                } else {
                    echo "Error: " . "1_". $result;
                    $con->close();
                }

            }
        }
        else {
            echo "Error: not_found";
            $con->close();
        }
    }


    function getData($con)
    {


        if ($con) {

            $sql = "SELECT locations.type, locations.address, locations.xcoord, locations.ycoord, locations.comment, locations.telephone, locations.email, locations.user, pictures.picture FROM locations left join pictures on locations.user = pictures.user and locations.picture = pictures.picture_id";



            $res = mysqli_query($con, $sql);

            $things = array();
            while ($row = mysqli_fetch_array($res)) {
                $thing = array(
                    "type" => $row['type'],
                    "address" => $row['address'],
                    "xcoord" => $row['xcoord'],
                    "ycoord" => $row['ycoord'],
                    "comment" => $row['comment'],
                    "telephone" => $row['telephone'],
                    "email" => $row['email'],
                    "user" => $row['user'],
                    "picture" => $row['picture'],
                );
                $things[] = $thing;
            }

            echo json_encode($things);



        }

        $con->close();

    }





    function login($con)
    {

        if (isset($_GET['username'])) {

            $username = mysqli_real_escape_string($con,$_GET['username']);
            $password = mysqli_real_escape_string($con,$_GET['password']);



            $usr = "SELECT * FROM users WHERE `username` = '$username' and `password` = '$password'";


            if (mysqli_query($con, $usr)) {


                $res = mysqli_query($con, $usr);
                $row = mysqli_fetch_array($res);

                $thing = array(
                    "user_id" => $row['user_id'],
                    "username" => $row['username'],
                    "password" => $row['password'],
                    "first_name" => $row['first_name'],
                    "last_name" => $row['last_name'],
                    "email" => $row['email'],
                    "active" => $row['active']
                );
                $i = 0;
                foreach ($thing as $x => $x_value) {
                    $val[$i] = $x_value;
                    $i = $i + 1;
                }


                if (empty($username) == true || empty($password) == true) {
                    echo('Please type your username and password');

                } elseif (strcasecmp($val[1], $username) != 0) {
                    echo('Wrong username or password');

                } else {

                    session_start();
                    $_SESSION['loggedin'] = true;
                    $_SESSION['username'] = $username;
                    $_SESSION['user_id'] = $val[0];


                    echo json_encode($thing);
                }

            }

        }


        $con->close();
    }


    function register($con) //
    {


        if (isset($_GET['username'])) {

            $username = mysqli_real_escape_string($con,$_GET['username']);
            $password1 = mysqli_real_escape_string($con,$_GET['password1']);
            $password2 = mysqli_real_escape_string($con,$_GET['password2']);
            $email = mysqli_real_escape_string($con,$_GET['email']);




            $usr = "SELECT * FROM users WHERE `username` = '$username' ";


            if (mysqli_query($con, $usr)) {


                $res = mysqli_query($con, $usr);
                $row = mysqli_fetch_array($res);

                $thing = array(
                    "user_id" => $row['user_id'],
                    "username" => $row['username'],
                    "password" => $row['password'],
                    "first_name" => $row['first_name'],
                    "last_name" => $row['last_name'],
                    "email" => $row['email'],
                    "active" => $row['active']
                );
                $i = 0;
                foreach ($thing as $x => $x_value) {
                    $val[$i] = $x_value;
                    $i = $i + 1;
                }


                if ((strlen($password1) < 8) && (!preg_match('/^[a-zA-Z0-9]{5,}$/', $username))) {
                    echo("Password must contains at least 8 symbols. Letters and numbers only.");
                } elseif (empty($username) == true || empty($password1) == true || empty($email) == true || empty($password2) == true) {
                    echo('Please fill in all fields');

                } elseif (strcasecmp($val[2], $username) == 0) {
                    echo('Username exists');

                } elseif ($password1 != $password2) {
                    echo('Passwords do not match');

                } elseif (!preg_match('/^[a-zA-Z0-9]{5,}$/', $username)) {
                    echo("Please use letters and numbers only");
                } elseif (strcasecmp($val[2], $username) == 0) {
                    echo('Username exists');

                } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    echo("Wrong email address");
                } else {


                    $sql = "INSERT INTO users (username, password, first_name, last_name, email, active)
                    VALUES ('$username', '$password1', '','','$email','1')";

                    if (mysqli_query($con, $sql)) {


                        $id = "SELECT user_id FROM users WHERE `username` = '$username' and `password` = '$password1'";

                        if (mysqli_query($con, $id)) {
                            $res = mysqli_query($con, $id);
                            $user_id = mysqli_fetch_object($res)->user_id;

                            session_start();
                            $_SESSION['loggedin'] = true;
                            $_SESSION['username'] = $username;

                            $_SESSION['user_id'] = $user_id;
                            echo(1);


                        }

                    } else echo("Something's wrong please try again later.");

                }


            }

        }


        $con->close();
    }


}

?>









