<?php

include_once 'db_config.php';

$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
if ($mysqli->connect_error) {
    $message = "error_unable to connect to database";
    echo "<script type='text/javascript'>alert('$message');</script>";
    exit();
}