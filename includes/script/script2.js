
var map;
var map2;
var marker;
var xcoord = "";
var ycoord ="";



window.onload=function(){
    initialize();
}






function initialize() {

    myCenter=new google.maps.LatLng(40.053425,-7.272561,3);

    var mapProp = {
        center:myCenter,
        zoom:4,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    map=new google.maps.Map(document.getElementById("googleMap"),mapProp);




     setTimeout( function(){resizingMap(map);} , 400);


}





//
function resizingMap(map) {
    if(typeof map =="undefined") return;
    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
}


function codeAddress() {
     initialize();




    document.getElementById("fields").innerHTML = "Please Drag the marker to desired location";




    var address = document.getElementById('address').value;
    var comment = document.getElementById('comment').value;
    var email = document.getElementById('email').value;
    var number = document.getElementById('number').value;
    if (document.getElementById('type1').checked) {
        var type = document.getElementById('type1').value;
    }
    if (document.getElementById('type2').checked) {
        var type = document.getElementById('type2').value;
    }
    var geocoder = new google.maps.Geocoder();


    geocoder.geocode({'address': address}, function (results, status) {

        if (status == google.maps.GeocoderStatus.OK) {

            map.setCenter(results[0].geometry.location);

            xcoord = results[0].geometry.location.lat();
            ycoord = results[0].geometry.location.lng();
            document.getElementById("address").value = results[0].formatted_address;
            marker = new google.maps.Marker({
                map: map,
                draggable: true,
                title: "Drag me!",
                icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png',
                position: results[0].geometry.location

            });



            marker.setMap(map);
            //src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Camera-icon.svg/2000px-Camera-icon.svg.png"
            var contentString =
                '<h3>' + type + '</h3>' +
                '<div class=""><img align="left" width="90" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Camera-icon.svg/2000px-Camera-icon.svg.png">' +
                '<b>Adress </b>' + address + '<br>' +
                '<b>Comment: </b>' + comment + ' <br>' +
                '<b>Email: </b>:' + email + ' <br>' +
                '<b>Phone: </b>:' + number + '<br><br></div>';


            var infowindow = new google.maps.InfoWindow({
                content: contentString

            });

            // infowindow.open(map, marker);

            map.setZoom(15);

        } else {
            alert('Wrong address! ' + status);
        }



        google.maps.event.addListener(marker, 'click', function () {
            // map.setZoom(17);
            // infowindow.open(map, marker);
        });


        google.maps.event.addListener(marker, 'dragend', function () {
            //updateMarkerStatus('Drag ended');


            geocoder.geocode({
                latLng: marker.getPosition()
            }, function (responses) {


                    if (responses && responses.length > 0) {
                        document.getElementById("address").value = responses[0].formatted_address;
                        xcoord = marker.getPosition().lat();
                        ycoord = marker.getPosition().lng();

                            document.getElementById("fields").innerHTML = 'Press "Save" to finish.';
                            document.getElementById('closeBtn').disabled = false;


                        } else {
                            document.getElementById("address").value = "No address";

                            document.getElementById("fields").innerHTML = "Current location has no address, please drag the marker to another location";

                        }

            });

        });

        google.maps.event.addListener(marker, 'dragstart', function (evt) {
            document.getElementById("address").value = '-----';
        });


    });


}




$(document).ready(function() {
    $('#closeBtn').click(function() {

        if ((document.getElementById('address').value != "" && document.getElementById('comment').value != ""
            && (document.getElementById('email').value != "" || document.getElementById('number').value != "") &&
            (document.getElementById('type2').checked)) ||
            (document.getElementById('address').value != "" && document.getElementById('comment').value != ""
                &&  document.getElementById('type1').checked)) {



            var address = document.getElementById('address').value;
            var comment = document.getElementById('comment').value;
            var email = document.getElementById('email').value;
            var number = document.getElementById('number').value;
            if (document.getElementById('type1').checked) {
                var type = document.getElementById('type1').value;
            }
            if (document.getElementById('type2').checked) {
                var type = document.getElementById('type2').value;
            }
            var user = "";
            $.ajax({
                url: '../includes/connections/functions.php',
                type: 'GET',
                data: {
                    type: type,
                    address: address,
                    xcoord: xcoord,
                    ycoord: ycoord,
                    comment: comment,
                    email: email,
                    telephone: number,
                    pic: pic,
                    funct: "insertData",

                },
                success: function(data) {
                    if(data == 1) {
                        window.location.href = '../public_html/loggedin.php';
                    }
                    else alert(data);

                },
                error: function() {
                    alert('error:_database');
                }

            });

        } else{
            document.getElementById("fields").innerHTML = "Please fill in comment and contact details.";

        }

    });

});





